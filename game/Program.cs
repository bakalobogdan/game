﻿using System;

namespace game
{
    sealed class Program
    {
        static void Main(string[] args)
        {
            bool exit = false;
            Game guessGame = InitNewGame();
            guessGame.Start().Wait();
            while (!exit)
            {
                Console.WriteLine("WHAT TO DO NEXT?\n1.Play again with the same players.\n2.Play new game.\n3.Exit");
                var result = ReadByte(1, 3);
                switch (result)
                {
                    case 1:
                        guessGame.Restart().Wait();
                        break;
                    case 2:
                        guessGame = InitNewGame();
                        guessGame.Start().Wait();
                        continue;
                    case 3:
                        exit = true;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        static Game InitNewGame()
        {
            var guessGame = new Game();
            Console.WriteLine("\t\t*********GUESS GAME*********\n\n");
            Console.WriteLine("Please specify the amount of participating players. (From 2 to 8)");
            var numberOfPlayers = ReadByte(2, 8);
            for (int i = 1; i <= numberOfPlayers; i++)
            {
                Console.WriteLine($"Please specify the name for player #{i}");
                var name = Console.ReadLine();
                Console.WriteLine($"Please specify the type of player {name}");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("1 - Random\n2 - Memory\n3 - Thorough\n4 - Cheater\n5 - Thorough Cheater");
                Console.ResetColor();
                var type = (PlayerType)ReadByte(1, 5);
                guessGame.RegisterPlayer(name, type);
            }
            return guessGame;
        }
        static byte ReadByte(byte? min = null, byte? max = null)
        {
            bool isValid = false;
            byte value = 0;
            while (!isValid)
            {
                var stringValue = Console.ReadLine();
                if (!byte.TryParse(stringValue, out value) || value < min || value > max)
                {
                    Console.WriteLine($"Incorrect input! Input should be the number between {min} and {max}");
                }
                else
                {
                    isValid = true;
                }
            }
            return value;
        }
    }
}
