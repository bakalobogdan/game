﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace game
{
    public sealed class Game
    {
        int BasketWeight { get; set; }
        Player Winner { get; set; }
        GameState State { get; set; }
        List<Player> Players { get; set; } = new List<Player>();
        IRandomGenerator RandomGenerator { get; set; } = new RandomGenerator();
        // lock for restricting simultaneous access to guesses list
        object guessAttemptsLock = new object();
        List<GuessAttempt> GuessAttempts { get; set; } = new List<GuessAttempt>();

        public void RegisterPlayer(string name, PlayerType type)
        {
            if(Players.Count < 8)
            {
                Players.Add(new Player(this, name, type));
            }
            else
            {
                throw new InvalidOperationException();
            }
        }
        public async Task Start()
        {
            State = GameState.InProgress;
            // generates basket's weight randomly
            BasketWeight = RandomGenerator.Generate(Constants.MIN_WEIGHT, Constants.MAX_WEIGHT);
            WriteMessage($"Basket weight is {BasketWeight} kilos");
            //using CancellationTokenSource to set timeout
            using (var cts = new CancellationTokenSource(TimeSpan.FromMilliseconds(Constants.TIMEOUT)))
            {
                cts.Token.Register(() =>
                {
                    Console.WriteLine("Time is out!");
                    Stop();
                });
                List<Task> tasks = new List<Task>();
                //starting play tasks in parralel
                Parallel.For(0, Players.Count, i => {
                    tasks.Add(Players[i].Play());
                });
                //awaiting tasks to complete
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
        }
        public async Task Restart()
        {
            ClearAttemptsQueue();
            Winner = null;
            await Start();
        }
        public void Stop()
        {
            if (State == GameState.InProgress)
            {
                State = GameState.Stopped;
                if (Winner != null)
                {
                    WriteMessage($"{Winner.Name} has won the game");
                    WriteMessage($"Number of attempts: {GuessAttempts.Count}");
                }
                else
                {
                    var closestAttempt = GetClosestAttempt();
                    if(closestAttempt != null)
                    {
                        WriteMessage($"Unfortunately, nobody guessed the basket weight.\n");
                        WriteMessage($"But the closest was {closestAttempt.Player.Name} with amount: {closestAttempt.Weight}");
                    }
                }
                WriteMessage("GAME OVER");
            }
        }

        /// <summary>
        /// Processes the attempt to guess the basket weight
        /// </summary>
        /// <param name="attempt"></param>
        /// <param name="allowDuplicate"></param>
        /// <returns></returns>
        internal GuessResult TakeATry(GuessAttempt attempt, bool allowDuplicate = true)
        {
            //lock prevents other threads to process attempts simultaneously
            lock (guessAttemptsLock)
            {
                var result = new GuessResult();
                var delta = Math.Abs(BasketWeight - attempt.Weight);
                if (delta == 0)
                {
                    result.Success = true;
                    Winner = attempt.Player;
                    Stop();
                }
                else
                {
                    if (GuessAttempts.Count >= Constants.NUMBER_OF_ATTEMPTS)
                    {
                        Console.WriteLine("Number of attempts exceeded!");
                        Stop();
                    }
                }
                GuessAttempts.Add(attempt);
                result.Delay = delta;
                return result;
            }
        }
        internal bool IsInProgress
        {
            get
            {
                return State == GameState.InProgress;
            }
        }
        /// <summary>
        /// Gets unique produced guesses
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<int> GetProducedGuesses()
        {
            lock(guessAttemptsLock)
            {
                return GuessAttempts.Select(x => x.Weight).Distinct();
            }
        }
        /// <summary>
        /// Gets first closest attempt.
        /// </summary>
        /// <returns></returns>
        GuessAttempt GetClosestAttempt()
        {
            lock (guessAttemptsLock)
            {
                if (GuessAttempts.Any())
                {
                    //get minimal delta between attempt's weight and real weight
                    int min = GuessAttempts.Min(x => Math.Abs(x.Weight - BasketWeight));
                    //get just first caught attempt with such delta because order is guaranteed
                    return GuessAttempts.First(x=> Math.Abs(x.Weight - BasketWeight) == min);
                }
                else
                    return null;
            }
        }
        void WriteMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"SYSTEM: {message}");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        void ClearAttemptsQueue()
        {
            lock (guessAttemptsLock)
            {
                GuessAttempts.Clear();
            }
        }
    }
    
    
}
