﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace game
{
    interface IPlayStrategy
    {
        Task Play(Player player);
    }
    sealed class RandomPlayStrategy : IPlayStrategy
    {
        private readonly IRandomGenerator randomGenerator;

        internal RandomPlayStrategy(IRandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }
        public async Task Play(Player player)
        {
            while(player.IsGameInProgress)
            {
                GuessResult result;
                var guess = randomGenerator.Generate(Constants.MIN_WEIGHT, Constants.MAX_WEIGHT);
                lock (GuessLocker.Instance)
                {
                    result = player.TryGuess(guess);
                }
                await player.ProcessGuessResult(result);
            }
        }
    }
    sealed class MemoryPlayStrategy : IPlayStrategy
    {
        private readonly IRandomGenerator randomGenerator;
        
        internal MemoryPlayStrategy(IRandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }
        public async Task Play(Player player)
        {
            List<int> avaliableGuesses = new List<int>(Enumerable.Range(Constants.MIN_WEIGHT, Constants.MAX_WEIGHT - Constants.MIN_WEIGHT + 1));
            while (player.IsGameInProgress)
            {
                GuessResult result;
                var index = randomGenerator.Generate(0, avaliableGuesses.Count);
                int guess = avaliableGuesses.RemoveAtAndGet(index);
                lock (GuessLocker.Instance)
                {
                    result = player.TryGuess(guess);
                }
                await player.ProcessGuessResult(result);
            }
        }
    }
    sealed class ThoroughPlayStrategy : IPlayStrategy
    {
        public async Task Play(Player player)
        {
            int guess = Constants.MAX_WEIGHT;
            while (player.IsGameInProgress && guess <= 140)
            {
                GuessResult result;
                lock(GuessLocker.Instance)
                {
                    result = player.TryGuess(guess++);
                }
                await player.ProcessGuessResult(result);
            }
        }
    }
    sealed class CheaterPlayStrategy : IPlayStrategy
    {
        private readonly IRandomGenerator randomGenerator;
        
        internal CheaterPlayStrategy(IRandomGenerator randomGenerator)
        {
            this.randomGenerator = randomGenerator;
        }
        public async Task Play(Player player)
        {
            while (player.IsGameInProgress)
            {
                GuessResult result;
                lock(GuessLocker.Instance)
                {
                    var avaliableGuesses = Enumerable.Range(Constants.MIN_WEIGHT, Constants.MAX_WEIGHT - Constants.MIN_WEIGHT + 1)
                        .Except(player.Game.GetProducedGuesses())
                        .ToList();
                    var index = randomGenerator.Generate(0, avaliableGuesses.Count);
                    int guess = avaliableGuesses[index];
                    result = player.TryGuess(guess);
                }
                await player.ProcessGuessResult(result);
            }
        }
    }
    sealed class ThoroughCheaterStrategy : IPlayStrategy
    {
        public async Task Play(Player player)
        {
            int guess = 40;
            while(player.IsGameInProgress && guess <= 140)
            {
                GuessResult result;
                lock (GuessLocker.Instance)
                {
                    if (player.Game.GetProducedGuesses().Any(x => x == guess))
                    {
                        guess++;
                        continue;
                    }
                    result = player.TryGuess(guess++);
                }
                await player.ProcessGuessResult(result);
            }
        }
    }
    sealed class GuessLocker
    {
        private static Lazy<object> locker = new Lazy<object>(()=>new object(), true);
        public static object Instance
        {
            get
            {
                return locker.Value;
            }
        }
    }
}