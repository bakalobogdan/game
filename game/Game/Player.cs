﻿using System;
using System.Threading.Tasks;

namespace game
{
    public sealed class Player
    {
        public string Name { get; set; }
        internal Game Game { get; set; }
        IPlayStrategy PlayStrategy { get; set; }

        public Player(Game game, string name, PlayerType type)
        {
            this.Game = game;
            this.Name = name;
            this.PlayStrategy = PlayStrategyFactory.GetPlayStrategy(type);
        }
        public async Task Play()
        {
            await PlayStrategy.Play(this);
        }
        internal void SetPlayStrategy(IPlayStrategy playStrategy)
        {
            this.PlayStrategy = playStrategy;
        }
        internal GuessResult TryGuess(int guess)
        {
            if(Game.IsInProgress)
            {
                //this.WriteMessage($"trying {guess}");
                return Game.TakeATry(new GuessAttempt(this, guess));
            }
            else
            {
                return new GuessResult { Success = false, Delay = 0 };
            }
        }
        internal async Task ProcessGuessResult(GuessResult result)
        {
            if (!result.Success && this.IsGameInProgress)
            {
                //this.WriteMessage($"waiting {result.Delay}");
                await Task.Delay(result.Delay);
            }
        }
        internal bool IsGameInProgress
        {
            get
            {
                return Game.IsInProgress;
            }
        }
        internal void WriteMessage(string message)
        {
            Console.WriteLine($"{Name}: {message}");
        }
    }
}
