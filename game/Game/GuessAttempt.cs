﻿namespace game
{
    sealed class GuessAttempt
    {
        internal GuessAttempt(Player player, int weight)
        {
            Player = player;
            Weight = weight;
        }
        internal Player Player { get; set; }
        internal int Weight { get; set; }
    }
}
