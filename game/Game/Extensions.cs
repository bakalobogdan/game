﻿using System.Collections.Generic;

namespace game
{
    public static class Extensions
    {
        public static T RemoveAtAndGet<T>(this IList<T> list, int index)
        {
            T value = list[index];
            list.RemoveAt(index);
            return value;
        }
    }
}
