﻿using System;

namespace game
{
    static class PlayStrategyFactory
    {
        static internal IPlayStrategy GetPlayStrategy(PlayerType type)
        {
            switch(type)
            {
                case PlayerType.Random:
                    return new RandomPlayStrategy(new RandomGenerator());
                case PlayerType.Memory:
                    return new MemoryPlayStrategy(new RandomGenerator());
                case PlayerType.Thorough:
                    return new ThoroughCheaterStrategy();
                case PlayerType.Cheater:
                    return new CheaterPlayStrategy(new RandomGenerator());
                case PlayerType.ThoroughCheater:
                    return new ThoroughCheaterStrategy();
                default:
                    throw new ArgumentOutOfRangeException(nameof(type));
            }
        }
    }
}