﻿namespace game
{
    sealed class GuessResult
    {
        public bool Success { get; set; }
        public int Delay { get; set; }
    }
}
