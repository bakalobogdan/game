﻿namespace game
{
    static internal class Constants
    {
        static internal readonly int TIMEOUT = 1500;
        static internal readonly int NUMBER_OF_ATTEMPTS = 100;
        static internal readonly int MIN_WEIGHT = 40;
        static internal readonly int MAX_WEIGHT = 140;
    }
}