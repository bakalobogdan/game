﻿using System;

namespace game
{
    public interface IRandomGenerator
    {
        int Generate(int min, int max);
    }
    class RandomGenerator : IRandomGenerator
    {
        readonly Random random = new Random(Guid.NewGuid().GetHashCode());
        public int Generate(int min, int max)
        {
            return random.Next(min, max);
        }
    }
}
